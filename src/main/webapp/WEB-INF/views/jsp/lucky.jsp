<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head><title>First JSP</title></head>
<body>

<p>Hi there! You are ${userData.firstName} ${userData.lastName}</p>
<p>
    e-mail: ${userData.email} <br>
    login: ${userData.login} <br>
    status: ${userData.status} <br>
    <c:forEach var="role" items="${userData.roles}" varStatus="i">
        ${i.index + 1} role: ${role} <br>
    </c:forEach>
    <c:forEach var="address" items="${userData.addresses}" varStatus="i">
        ${i.index + 1} address: ${address} <br>
    </c:forEach>
</p>

<p>

    <c:choose>
        <c:when test="${random>=0.9}">You will have a lucky day! </c:when>
        <c:otherwise> Well, life goes on ... </c:otherwise>
    </c:choose>
    <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${random * 100}"/> %

  <a href="lucky"><p>Try Again</p></a>
</body>
</html>