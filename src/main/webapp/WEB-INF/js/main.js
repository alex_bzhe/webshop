(function(){
    console.log("Hello World!");
})();

$(function() {
  var $form = $('#payForm');
  $form.submit(function(event) {
    // Отключим кнопку, чтобы предотвратить повторные клики
    $form.find('.submit').prop('disabled', true);
    // Запрашиваем token у Stripe
    Stripe.card.createToken($form, stripeResponseHandler);
    // Запретим форме submit
    return false;
  });
});

function stripeResponseHandler(status, response) {
  // Получим форму:
  var $form = $('#payForm');
  if (response.error) { // Problem!
    // Показываем ошибки в форме:
    $form.find('.payment-errors').text(response.error.message);
    $form.find('.submit').prop('disabled', false); // Разрешим submit
  } else { // Token был создан
    // Вставим token и card id в форму, чтобы при submit он пришел на сервер:
    $form.append($('<input type="hidden" name="stripeToken">').val(response.id));
    $form.append($('<input type="hidden" name="cardIdToken">').val(response.card.id));
    // Отключаем все остальные поля
//    $form.find('#cardNumber').prop('disabled', true);
//    $form.find('#expMonth').prop('disabled', true);
//    $form.find('#expYear').prop('disabled', true);
//    $form.find('#cardCvc').prop('disabled', true);
    // Сабмитим форму:
    $form.get(0).submit();
  }
};