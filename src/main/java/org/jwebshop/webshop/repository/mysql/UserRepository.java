package org.jwebshop.webshop.repository.mysql;


import org.jwebshop.webshop.entity.mysql.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);

    User findByLogin(String login);
}
