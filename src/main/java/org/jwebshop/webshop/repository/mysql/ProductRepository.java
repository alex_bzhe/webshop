package org.jwebshop.webshop.repository.mysql;

import org.jwebshop.webshop.entity.mysql.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface ProductRepository extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {

    @Query("FROM Product WHERE description_id.brief_en LIKE CONCAT('%',:briefEn, '%')")
    Collection<Product> findByBriefEn(@Param("briefEn") String briefEn);
}
