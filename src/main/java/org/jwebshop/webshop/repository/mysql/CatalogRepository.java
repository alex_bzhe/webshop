package org.jwebshop.webshop.repository.mysql;

import org.jwebshop.webshop.entity.mysql.Catalog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface CatalogRepository extends JpaRepository<Catalog, Long> {

    @Query("FROM Catalog " +
            "WHERE description_id.brief_en LIKE CONCAT('%',:briefEn, '%')")
    Collection<Catalog> findByBriefEn(@Param("briefEn") String briefEn);

}
