package org.jwebshop.webshop.repository.postgresql;

import org.jwebshop.webshop.entity.postgresql.Country;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CountryRepository extends JpaRepository<Country, Long> {

    List<Country> findByName(String countryName);
}
