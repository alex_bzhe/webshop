package org.jwebshop.webshop.repository.mongo.secondary;

import org.jwebshop.webshop.entity.mongo.Nomedia;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NomediaRepository extends MongoRepository<Nomedia, String> {

    List<Nomedia> findByOwnerId(Long ownerId);

    @Query("{ 'type' : { $regex: :#{#typeRegex} } }")
    List<Nomedia> findByTypeRegex(@Param("typeRegex") String typeRegex);
}