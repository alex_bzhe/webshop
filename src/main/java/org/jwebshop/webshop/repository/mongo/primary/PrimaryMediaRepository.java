package org.jwebshop.webshop.repository.mongo.primary;

import org.jwebshop.webshop.entity.mongo.Media;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PrimaryMediaRepository extends MongoRepository<Media, String> {

    List<Media> findByOwnerId(Long ownerId);
}
