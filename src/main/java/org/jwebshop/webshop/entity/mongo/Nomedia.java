package org.jwebshop.webshop.entity.mongo;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Document
public class Nomedia {

    @Id
    @ApiModelProperty(hidden = true)
    private String id;
//    @Indexed
    private Long ownerId;
    private String type;
    private String path;
}