package org.jwebshop.webshop.entity.mysql;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jwebshop.webshop.config.jackson.jsonviewannotation.ProductView;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Entity
public class Manufacturer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(ProductView.class)
    private Long id;

    @OneToOne(fetch = FetchType.EAGER, cascade= CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "description_id", nullable=true)
    @JsonView(ProductView.class)
    private Description description_id;

    @OneToMany(mappedBy = "manufacturer_id")
    private Set<Product> products;
}
