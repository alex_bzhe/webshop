package org.jwebshop.webshop.entity.mysql;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Embeddable
public class Address {

    private String zip;
    private String country;
    private String state;
    private String city;
    private String street;
    private String number;
}
