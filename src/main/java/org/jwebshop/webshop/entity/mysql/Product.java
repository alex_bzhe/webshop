package org.jwebshop.webshop.entity.mysql;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jwebshop.webshop.config.jackson.jsonviewannotation.CatalogView;
import org.jwebshop.webshop.config.jackson.jsonviewannotation.ProductView;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.util.Collection;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView({ProductView.class, CatalogView.class})
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade= CascadeType.ALL)
    @JoinColumn(name = "manufacturer_id", nullable=true)
    @JsonView({ProductView.class, CatalogView.class})
    private Manufacturer manufacturer_id;

    @OneToOne(fetch = FetchType.EAGER, cascade= CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "description_id", nullable=true)
    @JsonView({ProductView.class, CatalogView.class})
    private Description description_id;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "catalog_product",
            joinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "catalog_id", referencedColumnName = "id"))
    @JsonView(CatalogView.class)
    private Collection<Catalog> catalogs;

    @JsonView({ProductView.class, CatalogView.class})
    private Double price;
}