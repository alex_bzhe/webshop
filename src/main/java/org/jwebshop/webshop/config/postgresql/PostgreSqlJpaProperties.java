package org.jwebshop.webshop.config.postgresql;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

@Data
@ConfigurationProperties(prefix = "spring.datasource.postgresql")
public class PostgreSqlJpaProperties {
    private Map<String, String> jpa;
}
