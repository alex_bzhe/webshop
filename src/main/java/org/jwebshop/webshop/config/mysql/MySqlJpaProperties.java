package org.jwebshop.webshop.config.mysql;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

@Data
@ConfigurationProperties(prefix = "spring.datasource.mysql")
public class MySqlJpaProperties {
    private Map<String, String> jpa;
}
