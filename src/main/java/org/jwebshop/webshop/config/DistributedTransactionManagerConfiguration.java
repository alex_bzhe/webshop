package org.jwebshop.webshop.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.transaction.ChainedTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
public class DistributedTransactionManagerConfiguration {

    @Bean(name = "sqlDistributedTransactionManager")
    public ChainedTransactionManager sqlDistributedTransactionManager(
            @Qualifier("mysqlTransactionManager") PlatformTransactionManager mysqlTransactionManager,
            @Qualifier("postgresqlTransactionManager") PlatformTransactionManager postgresqlTransactionManager) {
        return new ChainedTransactionManager(mysqlTransactionManager, postgresqlTransactionManager);
    }

    @Bean(name = "mongoDistributedTransactionManager")
    public ChainedTransactionManager mongoDistributedTransactionManager(
            @Qualifier("primaryMongoTransactionManager") MongoTransactionManager primaryMongoTransactionManager,
            @Qualifier("secondaryMongoTransactionManager") MongoTransactionManager secondaryMongoTransactionManager) {
        return new ChainedTransactionManager(primaryMongoTransactionManager, secondaryMongoTransactionManager);
    }

    @Bean(name = "totalDistributedTransactionManager")
    public ChainedTransactionManager totalDistributedTransactionManager(
            @Qualifier("mysqlTransactionManager") PlatformTransactionManager mysqlTransactionManager,
            @Qualifier("postgresqlTransactionManager") PlatformTransactionManager postgresqlTransactionManager,
            @Qualifier("primaryMongoTransactionManager") MongoTransactionManager primaryMongoTransactionManager,
            @Qualifier("secondaryMongoTransactionManager") MongoTransactionManager secondaryMongoTransactionManager) {
        return new ChainedTransactionManager(
                mysqlTransactionManager,
                postgresqlTransactionManager,
                primaryMongoTransactionManager,
                secondaryMongoTransactionManager);
    }
}

//    Usage:
//
//    @Transactional(value = "sqlDistributedTransactionManager")
//    void insertIntoMultipleDBs() {
//            mysqlRepo.insert(...);
//            postgresqlRepo.insert(...);
//    }