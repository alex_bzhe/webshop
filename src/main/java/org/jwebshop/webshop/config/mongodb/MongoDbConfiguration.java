package org.jwebshop.webshop.config.mongodb;

import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDbFactory;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

import java.util.Arrays;

@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(MultipleMongoProperties.class)
public class MongoDbConfiguration {

    private final MultipleMongoProperties mongoProperties;

    @Bean(name = "primaryMongoTemplate")
    @Primary
    public MongoTemplate primaryMongoTemplate() {
        final MongoDbFactory primaryFactory = primaryFactory(mongoProperties.getPrimary());
        final MappingMongoConverter converter = mappingMongoConverter(primaryFactory);
        return new MongoTemplate(primaryFactory, converter);
    }

    @Bean
    @Primary
    public MongoDbFactory primaryFactory(final MongoProperties mongo) {
        return new SimpleMongoClientDbFactory(builderMongoClient(mongo), mongo.getDatabase());
    }

    @Bean(name = "primaryMongoTransactionManager")
    @Primary
    MongoTransactionManager primaryMongoTransactionManager() {
        return new MongoTransactionManager(primaryFactory(mongoProperties.getPrimary()));
    }

    @Bean(name = "secondaryMongoTemplate")
    public MongoTemplate secondaryMongoTemplate() {
        final MongoDbFactory secondaryFactory = secondaryFactory(mongoProperties.getSecondary());
        final MappingMongoConverter converter = mappingMongoConverter(secondaryFactory);
        return new MongoTemplate(secondaryFactory, converter);

    }

    @Bean
    public MongoDbFactory secondaryFactory(final MongoProperties mongo) {
        return new SimpleMongoClientDbFactory(builderMongoClient(mongo), mongo.getDatabase());
    }

    @Bean(name = "secondaryMongoTransactionManager")
    MongoTransactionManager secondaryMongoTransactionManager() {
        return new MongoTransactionManager(secondaryFactory(mongoProperties.getSecondary()));
    }

    private MongoClient builderMongoClient(final MongoProperties mongo) {
        MongoCredential credential = MongoCredential.createCredential(
                mongo.getUsername(), mongo.getAuthenticationDatabase(), mongo.getPassword());
        return MongoClients.create(MongoClientSettings.builder().credential(credential).applyToClusterSettings(
                builder -> builder.hosts(Arrays.asList(new ServerAddress(mongo.getHost(), mongo.getPort()))))
                .build());
    }

    private MappingMongoConverter mappingMongoConverter(MongoDbFactory mongoDbFactory) {
        MappingMongoConverter converter = new MappingMongoConverter(mongoDbFactory, new MongoMappingContext());
        converter.setTypeMapper(new DefaultMongoTypeMapper(null));
        return converter;
    }
}
