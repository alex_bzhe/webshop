package org.jwebshop.webshop.config.mongodb;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(
        basePackages = "org.jwebshop.webshop.repository.mongo.secondary",
        mongoTemplateRef = "secondaryMongoTemplate")
public class SecondaryMongoRepositoryConfiguration {
}
