package org.jwebshop.webshop.config.jackson.deserialization;

import com.fasterxml.jackson.databind.util.StdConverter;
import org.jwebshop.webshop.dto.form.ProductFilterForm;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class ProductFilterFormSanitizer extends StdConverter<ProductFilterForm, ProductFilterForm> {

    @Resource
    private CustomProductSortOptionDeserializer customProductSortOptionDeserializer;

    @Resource
    private CustomPageNumberDeserializer customPageNumberDeserializer;

    @Resource
    private CustomPageSizeDeserializer customPageSizeDeserializer;

    @Override
    public ProductFilterForm convert(ProductFilterForm productFilterForm) {
        if (productFilterForm.getProductSortOption() == null) {
            productFilterForm.setProductSortOption(customProductSortOptionDeserializer.getDefaultValue());
        }
        if (productFilterForm.getPageNumber() == null) {
            productFilterForm.setPageNumber(customPageNumberDeserializer.getDefaultValue());
        }
        if (productFilterForm.getPageSize() == null) {
            productFilterForm.setPageSize(customPageSizeDeserializer.getDefaultValue());
        }
        return productFilterForm;
    }
}
