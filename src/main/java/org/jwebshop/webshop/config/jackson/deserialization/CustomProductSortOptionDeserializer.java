package org.jwebshop.webshop.config.jackson.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.jwebshop.webshop.enumeration.ProductSortOption;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

@Component
public class CustomProductSortOptionDeserializer extends StdDeserializer<ProductSortOption> {

    public CustomProductSortOptionDeserializer() {
        this(null);
    }

    public CustomProductSortOptionDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public ProductSortOption deserialize(
            JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        final String productSortOptionString = jsonParser.getText();
        final Optional<ProductSortOption> productSortOption = Arrays.stream(ProductSortOption.values())
                .filter(option -> option.getValue().equals(productSortOptionString))
                .findFirst();
        return productSortOption.orElse(ProductSortOption.PRICE_ASC);
    }

    @Override
    public ProductSortOption getNullValue(DeserializationContext ctxt) {
        return ProductSortOption.PRICE_ASC;
    }

    public ProductSortOption getDefaultValue() {
        return ProductSortOption.PRICE_ASC;
    }
}
