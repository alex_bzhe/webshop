package org.jwebshop.webshop.config.jackson.deserialization;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
class IntDeserialazeHelper {

    private static final String CANNOT_USE = "Cannot use '%s' as %s, using value '%d'";

    static Integer parsePositiveIntOrDefault(String parsedIntString, Integer defaultIntValue, String intNameToLog) {
        try {
            int result = Integer.parseInt(parsedIntString);
            return result > 0 ? result : defaultIntValue;
        } catch (NumberFormatException e) {
            log.error(String.format(CANNOT_USE, parsedIntString, intNameToLog, defaultIntValue));
            return defaultIntValue;
        }
    }
}