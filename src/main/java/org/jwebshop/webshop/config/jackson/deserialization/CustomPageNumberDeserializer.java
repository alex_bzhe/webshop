package org.jwebshop.webshop.config.jackson.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static org.jwebshop.webshop.config.jackson.deserialization.IntDeserialazeHelper.parsePositiveIntOrDefault;

@Component
public class CustomPageNumberDeserializer extends JsonDeserializer<Integer> {

    private static final int PRE_DEFAULT_PAGE_NUMBER = 1;
    private static final String VARIABLE_NAME = "page number";

    @Value("${default.page.number}")
    private String defaultPageNumberString;

    @Override
    public Integer deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        final String pageNumberString = jsonParser.getText();
        return parsePositiveIntOrDefault(pageNumberString, getDefaultValue(), VARIABLE_NAME);
    }

    @Override
    public Integer getNullValue(DeserializationContext ctxt) {
        return getDefaultValue();
    }

    public Integer getDefaultValue() {
        return parsePositiveIntOrDefault(defaultPageNumberString, PRE_DEFAULT_PAGE_NUMBER, VARIABLE_NAME);
    }
}
