package org.jwebshop.webshop.config.jackson.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static org.jwebshop.webshop.config.jackson.deserialization.IntDeserialazeHelper.parsePositiveIntOrDefault;

@Component
public class CustomPageSizeDeserializer extends JsonDeserializer<Integer> {

    private static final int PRE_DEFAULT_PAGE_SIZE = 10;
    private static final String VARIABLE_NAME = "page size";

    @Value("${default.page.size}")
    private String defaultPageSizeString;

    @Override
    public Integer deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        final String pageSizeString = jsonParser.getText();
        return parsePositiveIntOrDefault(pageSizeString, getDefaultValue(), VARIABLE_NAME);
    }

    @Override
    public Integer getNullValue(DeserializationContext ctxt) {
        return getDefaultValue();
    }

    public Integer getDefaultValue() {
        return parsePositiveIntOrDefault(defaultPageSizeString, PRE_DEFAULT_PAGE_SIZE, VARIABLE_NAME);
    }
}