package org.jwebshop.webshop.config.jackson.deserialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Component
public class CustomDoubleDeserializer extends StdDeserializer<Double> {

    public CustomDoubleDeserializer() {
        this(null);
    }

    public CustomDoubleDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Double deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String price = jsonParser.getText();
        try {
            return Double.parseDouble(price);
        } catch (NumberFormatException | NullPointerException e) {
            log.error("Cannot parse Double from '" + price + "'");
        }
        return null;
    }
}
