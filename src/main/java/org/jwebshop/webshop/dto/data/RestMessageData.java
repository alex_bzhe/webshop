package org.jwebshop.webshop.dto.data;

import lombok.Data;
import lombok.Getter;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Data
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RestMessageData {

    private String message;
    private final int random;
    private static final int RANDOM_MAX_LIMIT = 100;

    public RestMessageData() {
        this.random = getRandomInt();
    }

    public RestMessageData(String message) {
        this();
        this.message = message;
    }

    private int getRandomInt() {
        return (int) (Math.random() * RANDOM_MAX_LIMIT);
    }
}
