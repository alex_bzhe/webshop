package org.jwebshop.webshop.dto.data;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class UserData {
    private String firstName;
    private String lastName;
    private String email;
    private String login;
    private List<String> roles;
    private String status;
    private List<String> addresses;
}
