package org.jwebshop.webshop.dto.converter.impl;

import org.apache.commons.lang3.StringUtils;
import org.jwebshop.webshop.dto.data.UserData;
import org.jwebshop.webshop.dto.converter.Converter;
import org.jwebshop.webshop.entity.mysql.Address;
import org.jwebshop.webshop.entity.mysql.User;
import org.jwebshop.webshop.enumeration.UserRole;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserDataConverter implements Converter<User, UserData> {

    @Override
    public UserData convertFrom(User user) {
        UserData userData = new UserData();
        copySourceFieldsToTargetWhenMatch(user, userData);
        userData.setRoles(user.getRoles().stream().map(UserRole::getValue).collect(Collectors.toList()));
        userData.setStatus(user.getStatus().getValue());
        userData.setAddresses(getUserAddressesAsStringList(user.getAddresses()));
        return userData;
    }

    private List<String> getUserAddressesAsStringList(final Collection<Address> addresses) {
        return addresses.stream()
                        .map(address -> StringUtils.join(Arrays.asList(
                                address.getZip(),
                                address.getCountry(),
                                address.getState(),
                                address.getCity(),
                                address.getStreet(),
                                address.getNumber()
                        ), ", "))
                        .collect(Collectors.toList());
    }
}
