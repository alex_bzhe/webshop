package org.jwebshop.webshop.dto.converter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public interface Converter<SOURCE, TARGET> {

    String SET = "set";
    String GET = "get";

    TARGET convertFrom(SOURCE source);

    default void copySourceFieldsToTargetWhenMatch(final SOURCE source, final TARGET target) {
        Objects.requireNonNull(source);
        Objects.requireNonNull(target);
        final Map<Method, Method> pairs = getMatchingSetterToGetterPairs(target.getClass(), source.getClass());
        pairs.forEach((setMethod, getMethod) -> {
            try {
                setMethod.invoke(target, getMethod.invoke(source));
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        });
    }

    default Map<Method, Method> getMatchingSetterToGetterPairs(final Class<?> targetClass, final Class<?> sourceClass) {
        final List<Method> targetSetMethods = getTargetSetMethods(targetClass);
        final Map<String, Method> sourceGetMethods = getSourceGetMethods(sourceClass);
        final Map<Method, Method> matchingSetterToGetterPairs = new HashMap<>();
        targetSetMethods.forEach(setMethod -> {
            final String fieldName = setMethod.getName().substring(SET.length());
            final Method getMethod = sourceGetMethods.get(fieldName);
            if (getMethod != null &&
                    getMethod.getReturnType().equals(setMethod.getParameterTypes()[0])) {
                matchingSetterToGetterPairs.put(setMethod, getMethod);
            }
        });
        return matchingSetterToGetterPairs;
    }

    default List<Method> getTargetSetMethods(final Class<?> targetClass) {
        return Arrays.stream(targetClass.getMethods())
                .filter(method -> method.getName().startsWith(SET))
                .collect(Collectors.toList());
    }

    default Map<String, Method> getSourceGetMethods(final Class<?> sourceClass) {
        return Arrays.stream(sourceClass.getMethods())
                .filter(method -> method.getName().startsWith(GET))
                .collect(Collectors.toMap(method -> method.getName().substring(GET.length()), method -> method));
    }
}
