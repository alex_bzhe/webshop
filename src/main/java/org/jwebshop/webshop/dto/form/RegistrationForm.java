package org.jwebshop.webshop.dto.form;

import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import org.jwebshop.webshop.validation.FieldMatch;

@Data
@NoArgsConstructor
@FieldMatch(first = "password", second = "confirmPassword", message = "The password fields must match")
public class RegistrationForm {

    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @Email
    @NotBlank
    private String email;
    @NotBlank
    private String login;
    @NotBlank
    private String password;
    @NotBlank
    private String confirmPassword;
}
