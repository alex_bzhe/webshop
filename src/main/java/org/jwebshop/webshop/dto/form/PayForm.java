package org.jwebshop.webshop.dto.form;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PayForm {

    private String stripeToken;
    private String cardIdToken;
    private String cardNumber;
    private String expMonth;
    private String expYear;
    private String cardCvc;
}
