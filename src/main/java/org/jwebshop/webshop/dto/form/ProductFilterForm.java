package org.jwebshop.webshop.dto.form;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jwebshop.webshop.config.jackson.deserialization.CustomPageNumberDeserializer;
import org.jwebshop.webshop.config.jackson.deserialization.CustomPageSizeDeserializer;
import org.jwebshop.webshop.config.jackson.deserialization.ProductFilterFormSanitizer;
import org.jwebshop.webshop.enumeration.ProductSortOption;
import org.jwebshop.webshop.config.jackson.deserialization.CustomDoubleDeserializer;
import org.jwebshop.webshop.config.jackson.deserialization.CustomProductSortOptionDeserializer;

@Data
@NoArgsConstructor
@JsonDeserialize(converter = ProductFilterFormSanitizer.class)
public class ProductFilterForm {

    private Long[] manufacturers;
    private String brief;
    @JsonDeserialize(using = CustomDoubleDeserializer.class)
    private Double minPrice;
    @JsonDeserialize(using = CustomDoubleDeserializer.class)
    private Double maxPrice;
    private Long[] catalogs;
    @JsonDeserialize(using = CustomProductSortOptionDeserializer.class)
    private ProductSortOption productSortOption;
    @JsonDeserialize(using = CustomPageNumberDeserializer.class)
    private Integer pageNumber;
    @JsonDeserialize(using = CustomPageSizeDeserializer.class)
    private Integer pageSize;
}
