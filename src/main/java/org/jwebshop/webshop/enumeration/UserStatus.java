package org.jwebshop.webshop.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum UserStatus {
    ACTIVE("ACTIVE"),
    BLOCKED("BLOCKED"),
    LIMITED("LIMITED");

    @Getter
    private String value;
}
