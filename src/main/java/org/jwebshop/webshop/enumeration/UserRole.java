package org.jwebshop.webshop.enumeration;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum UserRole {
    ADMIN("ADMIN"),
    USER("USER"),
    CUSTOMER("CUSTOMER");

    private static final String prefix = "ROLE_";

    private String value;

    public String getValue() {
        return prefix + value;
    }
}