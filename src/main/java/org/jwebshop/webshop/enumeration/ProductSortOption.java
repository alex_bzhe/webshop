package org.jwebshop.webshop.enumeration;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum ProductSortOption {
    PRICE_ASC("price_asc"),
    PRICE_DESC("price_desc"),
    DESCR_ASC("brief_asc"),
    DESCR_DESC("brief_desc");

    private String value;

    public String getValue() {
        return value;
    }
}
