package org.jwebshop.webshop.service;

import org.jwebshop.webshop.entity.postgresql.Country;

import java.util.List;

public interface CountryService {

    List<Country> findByName(String countryName);
}
