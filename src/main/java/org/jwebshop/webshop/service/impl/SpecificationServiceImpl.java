package org.jwebshop.webshop.service.impl;

import org.jwebshop.webshop.dto.form.ProductFilterForm;
import org.jwebshop.webshop.entity.mysql.Product;
import org.jwebshop.webshop.service.SpecificationService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

@Service
public class SpecificationServiceImpl implements SpecificationService {

    static final Map<String, Function<ProductFilterForm, Specification<Product>>> SPECS = new HashMap<>();

    static {
        SPECS.put("manufacturers", pff -> (r, q, c) ->
                c.in(r.get("manufacturer_id").get("id")).value(Arrays.asList(pff.getManufacturers())));
        SPECS.put("brief", pff -> (r, q, c) ->
                c.like(r.get("description_id").get("brief_en"), "%" + pff.getBrief() + "%"));
        SPECS.put("minPrice", pff -> (r, q, c) ->
                c.greaterThanOrEqualTo(r.get("price"), pff.getMinPrice()));
        SPECS.put("maxPrice", pff -> (r, q, c) ->
                c.lessThanOrEqualTo(r.get("price"), pff.getMaxPrice()));
        SPECS.put("catalogs", pff -> (r, q, c) ->
                c.in(r.joinCollection("catalogs").get("id")).value(Arrays.asList(pff.getCatalogs())));
    }

    @Override
    public Specification<Product> getProductSpecifications(final ProductFilterForm productFilterForm) {
        return Arrays.stream(ProductFilterForm.class.getDeclaredFields())
                .filter(field -> fieldValueIsPresent(field, productFilterForm))
                .map(field -> SPECS.get(field.getName()))
                .filter(Objects::nonNull)
                .map(function -> function.apply(productFilterForm))
                .reduce((r, q, c) -> c.conjunction(), Specification::and);
    }

    boolean fieldValueIsPresent(final Field field, final Object object) {
        field.setAccessible(true);
        try {
            return field.get(object) != null;
        } catch (IllegalAccessException | IllegalArgumentException ignored) {}
        return false;
    }
}