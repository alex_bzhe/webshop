package org.jwebshop.webshop.service.impl;

import org.jwebshop.webshop.entity.mongo.Nomedia;
import org.jwebshop.webshop.repository.mongo.secondary.NomediaRepository;
import org.jwebshop.webshop.service.NomediaService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class NomediaServiceImpl implements NomediaService {

    @Resource
    protected NomediaRepository nomediaRepository;

    @Override
    public List<Nomedia> findByOwnerId(Long ownerId) {
        return nomediaRepository.findByOwnerId(ownerId);
    }

    @Override
    public void saveNomedia(Nomedia nomedia) {
        nomediaRepository.save(nomedia);
    }

    @Override
    public List<Nomedia> findByTypeRegex(String typeRegex) {
        return nomediaRepository.findByTypeRegex(typeRegex);
    }
}