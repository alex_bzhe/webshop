package org.jwebshop.webshop.service.impl;

import org.jwebshop.webshop.entity.mysql.Address;
import org.jwebshop.webshop.entity.mysql.User;
import org.jwebshop.webshop.dto.form.RegistrationForm;
import org.jwebshop.webshop.enumeration.UserRole;
import org.jwebshop.webshop.enumeration.UserStatus;
import org.jwebshop.webshop.repository.mysql.UserRepository;
import org.jwebshop.webshop.service.UserService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserRepository userRepository;

    @Resource
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public User findByEmail(final String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User findByLogin(final String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public User save(final RegistrationForm registrationForm) {
        return userRepository.save(User.builder()
                .firstName(registrationForm.getFirstName())
                .lastName(registrationForm.getLastName())
                .email(registrationForm.getEmail())
                .login(registrationForm.getLogin())
                .password(passwordEncoder.encode(registrationForm.getPassword()))
                .roles(Collections.singleton(UserRole.CUSTOMER))
                .status(UserStatus.ACTIVE)
                .addresses(Collections.singleton(
                        Address.builder()
                                .zip("111111")
                                .country("none")
                                .state("none")
                                .city("none")
                                .street("none")
                                .number("1")
                                .build()))
                .build());
    }

    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(),
                user.getPassword(),
                mapRolesToAuthorities(user.getRoles()));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(final Collection<UserRole> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getValue()))
                .collect(Collectors.toList());
    }
}