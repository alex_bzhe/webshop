package org.jwebshop.webshop.service.impl;

import org.jwebshop.webshop.entity.mysql.Catalog;
import org.jwebshop.webshop.repository.mysql.CatalogRepository;
import org.jwebshop.webshop.service.CatalogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;

@Service
public class CatalogServiceImpl implements CatalogService {

    @Resource
    CatalogRepository catalogRepository;


    @Override
    public Collection<Catalog> findByBriefEn(String briefEn) {
        return catalogRepository.findByBriefEn(briefEn);
    }
}
