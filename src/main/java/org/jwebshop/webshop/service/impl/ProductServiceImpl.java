package org.jwebshop.webshop.service.impl;

import org.jwebshop.webshop.dto.form.ProductFilterForm;
import org.jwebshop.webshop.entity.mysql.Product;
import org.jwebshop.webshop.repository.mysql.ProductRepository;
import org.jwebshop.webshop.service.ProductService;
import org.jwebshop.webshop.service.SpecificationService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.jwebshop.webshop.enumeration.ProductSortOption.*;

@Service
public class ProductServiceImpl implements ProductService {

    protected static final Map<String, Sort> SORTS = new HashMap<>();

    static {
        SORTS.put(PRICE_ASC.getValue(), Sort.by("price").ascending());
        SORTS.put(PRICE_DESC.getValue(), Sort.by("price").descending());
        SORTS.put(DESCR_ASC.getValue(), Sort.by("description_id.brief_en").ascending());
        SORTS.put(DESCR_DESC.getValue(), Sort.by("description_id.brief_en").descending());
    }

    @Resource
    protected ProductRepository productRepository;

    @Resource
    protected SpecificationService specificationService;

    @Override
    public Collection<Product> findByBriefEn(String briefEn) {
        return productRepository.findByBriefEn(briefEn);
    }

    @Override
    public Collection<Product> findByProductFilterForm(ProductFilterForm productFilterForm) {
        final Integer pageNumber = productFilterForm.getPageNumber() - 1;
        final Integer pageSize = productFilterForm.getPageSize();
        final Sort sort = SORTS.getOrDefault(
                productFilterForm.getProductSortOption().getValue(), Sort.by("price").ascending());
        final Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
        final Specification<Product> specification = specificationService.getProductSpecifications(productFilterForm);
        return productRepository.findAll(specification, pageable).getContent();
    }
}
