package org.jwebshop.webshop.service.impl;

import org.jwebshop.webshop.entity.mongo.Media;
import org.jwebshop.webshop.repository.mongo.primary.PrimaryMediaRepository;
import org.jwebshop.webshop.repository.mongo.secondary.SecondaryMediaRepository;
import org.jwebshop.webshop.service.MediaService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class MediaServiceImpl implements MediaService {

    @Resource
    protected PrimaryMediaRepository primaryMediaRepository;

    @Resource
    protected SecondaryMediaRepository secondaryMediaRepository;

    @Override
    public List<Media> findByOwnerId(Integer repository, Long ownerId) {
        if (repository != null && repository > 0) {
            return secondaryMediaRepository.findByOwnerId(ownerId);
        } else {
            return primaryMediaRepository.findByOwnerId(ownerId);
        }
    }

    @Override
    public void saveMedia(Integer repository, Media media) {
        if (repository != null && repository > 0) {
            secondaryMediaRepository.save(media);
        } else {
            primaryMediaRepository.save(media);
        }
    }
}
