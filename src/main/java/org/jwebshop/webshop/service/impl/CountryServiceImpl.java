package org.jwebshop.webshop.service.impl;

import org.jwebshop.webshop.entity.postgresql.Country;
import org.jwebshop.webshop.repository.postgresql.CountryRepository;
import org.jwebshop.webshop.service.CountryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    @Resource
    protected CountryRepository countryRepository;

    @Override
    public List<Country> findByName(String countryName) {
        return countryRepository.findByName(countryName);
    }
}
