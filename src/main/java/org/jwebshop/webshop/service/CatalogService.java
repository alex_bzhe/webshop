package org.jwebshop.webshop.service;

import org.jwebshop.webshop.entity.mysql.Catalog;

import java.util.Collection;

public interface CatalogService {
    Collection<Catalog> findByBriefEn(String briefEn);
}
