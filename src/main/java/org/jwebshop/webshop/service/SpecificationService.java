package org.jwebshop.webshop.service;

import org.jwebshop.webshop.dto.form.ProductFilterForm;
import org.jwebshop.webshop.entity.mysql.Product;
import org.springframework.data.jpa.domain.Specification;

public interface SpecificationService {
    Specification<Product> getProductSpecifications(ProductFilterForm productFilterForm);
}
