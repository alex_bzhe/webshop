package org.jwebshop.webshop.service;

import org.jwebshop.webshop.entity.mongo.Media;

import java.util.List;

public interface MediaService {
    List<Media> findByOwnerId(Integer repository, Long ownerId);
    void saveMedia(Integer repository, Media media);
}
