package org.jwebshop.webshop.service;

import org.jwebshop.webshop.entity.mongo.Nomedia;

import java.util.List;

public interface NomediaService {
    List<Nomedia> findByOwnerId(Long ownerId);
    void saveNomedia(Nomedia nomedia);
    List<Nomedia> findByTypeRegex(String typeRegex);
}
