package org.jwebshop.webshop.service;

import org.jwebshop.webshop.entity.mysql.User;
import org.jwebshop.webshop.dto.form.RegistrationForm;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User findByLogin(String login);

    User save(RegistrationForm registration);
}
