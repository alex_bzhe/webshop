package org.jwebshop.webshop.service;

import org.jwebshop.webshop.dto.form.ProductFilterForm;
import org.jwebshop.webshop.entity.mysql.Product;

import java.util.Collection;

public interface ProductService {
    Collection<Product> findByBriefEn(String briefEn);
    Collection<Product> findByProductFilterForm(ProductFilterForm productFilterForm);
}
