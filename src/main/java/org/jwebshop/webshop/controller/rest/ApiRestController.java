package org.jwebshop.webshop.controller.rest;

import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.jwebshop.webshop.dto.data.RestMessageData;
import org.jwebshop.webshop.dto.form.ProductFilterForm;
import org.jwebshop.webshop.entity.mongo.Media;
import org.jwebshop.webshop.entity.mongo.Nomedia;
import org.jwebshop.webshop.entity.mysql.Catalog;
import org.jwebshop.webshop.entity.mysql.Product;
import org.jwebshop.webshop.config.jackson.jsonviewannotation.CatalogView;
import org.jwebshop.webshop.config.jackson.jsonviewannotation.ProductView;
import org.jwebshop.webshop.entity.postgresql.Country;
import org.jwebshop.webshop.service.CatalogService;
import org.jwebshop.webshop.service.CountryService;
import org.jwebshop.webshop.service.MediaService;
import org.jwebshop.webshop.service.NomediaService;
import org.jwebshop.webshop.service.ProductService;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Collection;

@Api(description = "REST APIs to test different Spring Data JPA repositories")
@RestController
@RequestMapping("/api")
public class ApiRestController {

    @Resource
    protected ProductService productService;

    @Resource
    protected CatalogService catalogService;

    @Resource
    protected ObjectFactory<RestMessageData> restMessageDataObjectFactory;

    @Resource
    protected MediaService mediaService;

    @Resource
    protected NomediaService nomediaService;

    @Resource
    protected CountryService countryService;

    @ApiOperation(value = "Just replies with JSON containing" +
            " 'hello rest ' added before the sent string value" +
            " and random int <= 100 [to see the response is unique]",
            authorizations = {@Authorization("ROLE_USER")})
    @Secured("ROLE_USER")
    @GetMapping("/{status}")
    public RestMessageData hello(
            @ApiParam(value = "String to return back with greetings", required = true)
            @PathVariable final String status) {
        final RestMessageData restMessageData = restMessageDataObjectFactory.getObject();
        restMessageData.setMessage("hello rest " + status);
        return restMessageData;
    }

    @Secured("ROLE_CUSTOMER")
    @GetMapping("/product/{briefEn}")
    @JsonView(ProductView.class)
    public Collection<Product> findProductsByBriefEnDescription(@PathVariable String briefEn) {
        return productService.findByBriefEn(briefEn);
    }

    @Secured("ROLE_CUSTOMER")
    @PostMapping("/product/filter")
    @JsonView(ProductView.class)
    public Collection<Product> filterProducts(@RequestBody ProductFilterForm productFilterForm) {
        return productService.findByProductFilterForm(productFilterForm);
    }

    @Secured("ROLE_CUSTOMER")
    @GetMapping("/catalog/{briefEn}")
    @JsonView(CatalogView.class)
    public Collection<Catalog> findCatalogsByBriefEnDescription(@PathVariable String briefEn) {
        return catalogService.findByBriefEn(briefEn);
    }

    @Secured("ROLE_CUSTOMER")
    @GetMapping("/media/{ownerId}/{repositoryId}")
    public Collection<Media> findMediaByOwnerIdInTheSpecifiedRepository(
            @PathVariable Integer repositoryId, @PathVariable Long ownerId) {
        return mediaService.findByOwnerId(repositoryId, ownerId);
    }

    @Secured("ROLE_CUSTOMER")
    @GetMapping("/nomedia/{ownerId}")
    public Collection<Nomedia> findNomediaByOwnerId(@PathVariable Long ownerId) {
        return nomediaService.findByOwnerId(ownerId);
    }

    @Secured("ROLE_CUSTOMER")
    @PostMapping("/media/{repository}")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveMediaToTheSpecifiedRepository(
            @PathVariable Integer repositoryId, @RequestBody Media media) {
        mediaService.saveMedia(repositoryId, media);
    }

    @Secured("ROLE_CUSTOMER")
    @PostMapping("/nomedia")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveNomedia(@RequestBody Nomedia nomedia) {
        nomediaService.saveNomedia(nomedia);
    }

    @Secured("ROLE_CUSTOMER")
    @GetMapping("/nomedia/findbytyperegex/{typeRegex}")
    public Collection<Nomedia> findByTypeRegex(@PathVariable String typeRegex) {
        return nomediaService.findByTypeRegex(typeRegex);
    }

    @Secured("ROLE_CUSTOMER")
    @GetMapping("/country/findbyname/{countryName}")
    public Collection<Country> findCountryByName(@PathVariable String countryName) {
        return countryService.findByName(countryName);
    }
}