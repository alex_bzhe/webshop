package org.jwebshop.webshop.controller.web.thymeleaf;

import org.jwebshop.webshop.entity.mysql.User;
import org.jwebshop.webshop.dto.form.RegistrationForm;
import org.jwebshop.webshop.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.validation.Valid;

@Controller
@RequestMapping("/registration")
public class RegistrationController {

    @Resource
    private UserService userService;

    @ModelAttribute("registrationForm")
    public RegistrationForm registrationForm() {
        return new RegistrationForm();
    }

    @GetMapping
    public String showRegistrationForm(Model model) {
        return "thyme/registration";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("registrationForm") @Valid final RegistrationForm registrationForm,
                                      final BindingResult result) {

        final User existingEmail = userService.findByEmail(registrationForm.getEmail());
        if (existingEmail != null) {
            result.rejectValue("email", null, "There is already an account registered with that email");
        }

        final User existingLogin = userService.findByLogin(registrationForm.getLogin());
        if (existingLogin != null) {
            result.rejectValue("login", null, "There is already an account registered with that login");
        }

        if (result.hasErrors()) {
            return "thyme/registration";
        }

        userService.save(registrationForm);
        return "redirect:/thyme/hello";
    }
}
