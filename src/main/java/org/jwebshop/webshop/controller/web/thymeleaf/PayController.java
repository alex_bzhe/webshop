package org.jwebshop.webshop.controller.web.thymeleaf;

import org.jwebshop.webshop.dto.form.PayForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/pay")
public class PayController {

    @GetMapping
    public String showPayForm() {
        return "thyme/payform";
    }

    @PostMapping
    public String getPayment(@ModelAttribute("payForm") @Valid final PayForm payForm,
                                      final BindingResult result, Model model) {
        model.addAttribute("payForm", payForm);
        return "thyme/gotpaid";
    }
}
