package org.jwebshop.webshop.controller.web.jsp;

import org.jwebshop.webshop.dto.converter.impl.UserDataConverter;
import org.jwebshop.webshop.dto.data.UserData;
import org.jwebshop.webshop.entity.mysql.User;
import org.jwebshop.webshop.service.UserService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;

@Controller
public class LuckyController {

    @Resource
    protected UserService userService;

    @Resource
    protected UserDataConverter userDataConverter;

    @Secured({"ROLE_CUSTOMER", "ROLE_ADMIN"})
    @GetMapping("/lucky")
    public String hello(Model model, Authentication auth) {
        final User user = userService.findByEmail(auth.getName());
        final UserData userData = userDataConverter.convertFrom(user);
        model.addAttribute("userData", userData);
        final double random = Math.random();
        model.addAttribute("random", random);
        return "jsp/lucky";
    }
}
