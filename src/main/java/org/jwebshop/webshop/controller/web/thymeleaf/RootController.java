package org.jwebshop.webshop.controller.web.thymeleaf;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RootController {

    @Secured({"ROLE_CUSTOMER", "ROLE_ADMIN"})
    @GetMapping({"/", "/index"})
    public String root() {
        return "thyme/index";
    }
}
