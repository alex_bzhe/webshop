
DROP DATABASE IF EXISTS `shop`;
CREATE DATABASE `shop` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'shop'@'localhost' IDENTIFIED BY 'shop';
GRANT ALL PRIVILEGES ON `shop`.* TO 'shop'@'localhost';
-- --------------------------------------------------------------
CREATE TABLE `shop`.`country` (

    `id` 				BIGINT(20) NOT NULL AUTO_INCREMENT,
	`name` 		        VARCHAR(20) NOT NULL,

    CONSTRAINT `country_id` PRIMARY KEY(`id`),
    INDEX `country_name_index`(`name`)
);
-- --------------------------------------------------------------
INSERT INTO `shop`.`country` VALUES(DEFAULT, 'United Kingdom');
INSERT INTO `shop`.`country` VALUES(DEFAULT, 'United States');
INSERT INTO `shop`.`country` VALUES(DEFAULT, 'Ukraine');
-- --------------------------------------------------------------

CREATE ROLE shop WITH
	LOGIN
	SUPERUSER
	CREATEDB
	NOCREATEROLE
	NOINHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD 'xxxxxx';

CREATE DATABASE shopdata
    WITH
    OWNER = shop
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1251'
    LC_CTYPE = 'English_United States.1251'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

CREATE UNIQUE INDEX country_name_index
    ON public.country USING btree
    (name COLLATE pg_catalog."en-US-x-icu" varchar_ops ASC NULLS LAST);

