
-- --------------------------------------------------------------
DROP TABLE IF EXISTS `jwshop`.`user`;
CREATE TABLE `jwshop`.`user` (

    `id` 				BIGINT(20) NOT NULL AUTO_INCREMENT,
	`first_name` 		VARCHAR(20) NOT NULL,
    `last_name` 		VARCHAR(20) NOT NULL,
    `email`		 		VARCHAR(50) NOT NULL UNIQUE,
    `login` 			VARCHAR(15) NOT NULL UNIQUE,
    `password` 			VARCHAR(255) NOT NULL,
    `status`            INT(1),

    CONSTRAINT `user_id` PRIMARY KEY(`id`),
    INDEX `login_index`(`login`),
    INDEX `email_index`(`email`)
);
-- --------------------------------------------------------------
INSERT INTO `jwshop`.`user` VALUES(DEFAULT, 'admin', 'admin', 'admin@admin.com', 'admin', '$2a$10$.PnnLwr04XIovgiiYpDZk.qXrkXMfx83AvDudSjMCmsBDaUHILAy6', 0);
INSERT INTO `jwshop`.`user` VALUES(DEFAULT, 'user', 'user', 'user@user.com', 'user', '$2a$10$xOPzBDZ3R1f19pUOPXjy8e10IDKRnc6jQl1keX6IQJI6L75cJBxZS', 0);
INSERT INTO `jwshop`.`user` VALUES(DEFAULT, 'customer', 'customer', 'customer@customer.com', 'customer', '$2a$10$TeWeNx3keDApvV8YWTlVY.4gREjwY8fz1WFbuiLwQUAgtLc14lvVm', 0);
INSERT INTO `jwshop`.`user` VALUES(DEFAULT, 'Joe', 'Smith', 'joe@smith.com', 'JoeSmith', '$2a$10$9ItAoVQI4wkRxtffN7RgsOVUPd.kfPZNdfiXYJBphDP/cKxgHKPF2', 0);
-- --------------------------------------------------------------