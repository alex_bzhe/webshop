
-- --------------------------------------------------------------
 DROP DATABASE IF EXISTS `jwshop`;
 CREATE DATABASE `jwshop` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
-- CREATE USER 'jwshop'@'localhost' IDENTIFIED BY 'jwshop';
 GRANT ALL PRIVILEGES ON `jwshop`.* TO 'jwshop'@'localhost';
 -- --------------------------------------------------------------
 SOURCE `C:\webshop\src\main\java\org\jwebshop\webshop\sqltables\user.sql`
 SOURCE `C:\webshop\src\main\java\org\jwebshop\webshop\sqltables\user_address.sql`
