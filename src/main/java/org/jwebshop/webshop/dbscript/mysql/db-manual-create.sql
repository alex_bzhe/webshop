DROP DATABASE IF EXISTS `jwshop`;
CREATE DATABASE `jwshop` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
-- CREATE USER 'jwshop'@'localhost' IDENTIFIED BY 'jwshop';
GRANT ALL PRIVILEGES ON `jwshop`.* TO 'jwshop'@'localhost';
-- --------------------------------------------------------------
CREATE TABLE `jwshop`.`user` (

    `id` 				BIGINT(20) NOT NULL AUTO_INCREMENT,
	`first_name` 		VARCHAR(20) NOT NULL,
    `last_name` 		VARCHAR(20) NOT NULL,
    `email`		 		VARCHAR(50) NOT NULL UNIQUE,
    `login` 			VARCHAR(15) NOT NULL UNIQUE,
    `password` 			VARCHAR(255) NOT NULL,
    `status`            INT(1),

    CONSTRAINT `user_id` PRIMARY KEY(`id`),
    INDEX `login_index`(`login`),
    INDEX `email_index`(`email`)
);
-- --------------------------------------------------------------
INSERT INTO `jwshop`.`user` VALUES(DEFAULT, 'admin', 'admin', 'admin@admin.com', 'admin', '$2a$10$.PnnLwr04XIovgiiYpDZk.qXrkXMfx83AvDudSjMCmsBDaUHILAy6', 0);
INSERT INTO `jwshop`.`user` VALUES(DEFAULT, 'user', 'user', 'user@user.com', 'user', '$2a$10$xOPzBDZ3R1f19pUOPXjy8e10IDKRnc6jQl1keX6IQJI6L75cJBxZS', 0);
INSERT INTO `jwshop`.`user` VALUES(DEFAULT, 'customer', 'customer', 'customer@customer.com', 'customer', '$2a$10$TeWeNx3keDApvV8YWTlVY.4gREjwY8fz1WFbuiLwQUAgtLc14lvVm', 0);
INSERT INTO `jwshop`.`user` VALUES(DEFAULT, 'Joe', 'Smith', 'joe@smith.com', 'JoeSmith', '$2a$10$9ItAoVQI4wkRxtffN7RgsOVUPd.kfPZNdfiXYJBphDP/cKxgHKPF2', 0);
-- --------------------------------------------------------------
CREATE TABLE `jwshop`.`user_role` (

  `user_id`             BIGINT(20) NOT NULL,
  `user_role_ordinal`        INT(1) NOT NULL,

  CONSTRAINT `user_role_pk` PRIMARY KEY(`user_id`, `user_role_ordinal`),
  CONSTRAINT `user_id_fk` FOREIGN KEY(`user_id`)
      REFERENCES `jwshop`.`user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
-- --------------------------------------------------------------
INSERT INTO `jwshop`.`user_role` VALUES(1, 0);
INSERT INTO `jwshop`.`user_role` VALUES(2, 1);
INSERT INTO `jwshop`.`user_role` VALUES(3, 2);
INSERT INTO `jwshop`.`user_role` VALUES(4, 2);
INSERT INTO `jwshop`.`user_role` VALUES(4, 1);
-- --------------------------------------------------------------
-- --------------------------------------------------------------
CREATE TABLE `jwshop`.`description` (

  `id`                  BIGINT(20) NOT NULL AUTO_INCREMENT,
  `brief_en`            VARCHAR(64),
  `brief_ru`            VARCHAR(64),
  `short_en`            VARCHAR(512),
  `short_ru`            VARCHAR(512),
  `full_en`             VARCHAR(2048),
  `full_ru`             VARCHAR(2048),

  CONSTRAINT `description_id` PRIMARY KEY(`id`)
);
-- --------------------------------------------------------------
INSERT INTO `jwshop`.`description` VALUES(DEFAULT, 'Ford', 'Форд', NULL, NULL, NULL, NULL);
INSERT INTO `jwshop`.`description` VALUES(DEFAULT, 'Opel', 'Опель', NULL, NULL, NULL, NULL);
INSERT INTO `jwshop`.`description` VALUES(DEFAULT, 'Cars', 'Легковые автомобили', NULL, NULL, NULL, NULL);
INSERT INTO `jwshop`.`description` VALUES(DEFAULT, 'Opel Cars', 'Легковые автомобили Опель', NULL, NULL, NULL, NULL);
INSERT INTO `jwshop`.`description` VALUES(DEFAULT, 'Ford Cars', 'Легковые автомобили Форд', NULL, NULL, NULL, NULL);
INSERT INTO `jwshop`.`description` VALUES(DEFAULT, 'Opel Kadett', 'Опель Кадет', NULL, NULL, NULL, NULL);
INSERT INTO `jwshop`.`description` VALUES(DEFAULT, 'Ford Fiesta', 'Форд Фиеста', NULL, NULL, NULL, NULL);
INSERT INTO `jwshop`.`description` VALUES(DEFAULT, 'Ford Fusion', 'Форд Фьюжн', NULL, NULL, NULL, NULL);
-- --------------------------------------------------------------
-- --------------------------------------------------------------
CREATE TABLE `jwshop`.`manufacturer` (

  `id`                  BIGINT(20) NOT NULL AUTO_INCREMENT,
  `description_id`      BIGINT(20),

  CONSTRAINT `manufacturer_id` PRIMARY KEY(`id`),
  CONSTRAINT `manufacturer_description_id` FOREIGN KEY(`description_id`)
      REFERENCES `jwshop`.`description`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
-- --------------------------------------------------------------
INSERT INTO `jwshop`.`manufacturer` VALUES(DEFAULT, 1);
INSERT INTO `jwshop`.`manufacturer` VALUES(DEFAULT, 2);
-- --------------------------------------------------------------
-- --------------------------------------------------------------
CREATE TABLE `jwshop`.`catalog` (

  `id`                  BIGINT(20) NOT NULL AUTO_INCREMENT,
  `description_id`      BIGINT(20),

  CONSTRAINT `catalog_id` PRIMARY KEY(`id`),
  CONSTRAINT `catalog_description_id` FOREIGN KEY(`description_id`)
      REFERENCES `jwshop`.`description`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
-- --------------------------------------------------------------
INSERT INTO `jwshop`.`catalog` VALUES(DEFAULT, 3);
INSERT INTO `jwshop`.`catalog` VALUES(DEFAULT, 4);
INSERT INTO `jwshop`.`catalog` VALUES(DEFAULT, 5);
-- --------------------------------------------------------------
-- --------------------------------------------------------------
CREATE TABLE `jwshop`.`catalog_catalog` (

  `parent_id`           BIGINT(20) NOT NULL,
  `child_id`            BIGINT(20) NOT NULL,

  CONSTRAINT `parent_id` FOREIGN KEY(`parent_id`)
      REFERENCES `jwshop`.`catalog`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `child_id` FOREIGN KEY(`child_id`)
      REFERENCES `jwshop`.`catalog`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
-- --------------------------------------------------------------
INSERT INTO `jwshop`.`catalog_catalog` VALUES(1, 2);
INSERT INTO `jwshop`.`catalog_catalog` VALUES(1, 3);
-- --------------------------------------------------------------
-- --------------------------------------------------------------
CREATE TABLE `jwshop`.`product` (

  `id`                  BIGINT(20) NOT NULL AUTO_INCREMENT,
  `manufacturer_id`     BIGINT(20),
  `description_id`      BIGINT(20),
  `price`               DECIMAL(9,2),

  CONSTRAINT `product_id` PRIMARY KEY(`id`),
  CONSTRAINT `product_description_id` FOREIGN KEY(`description_id`)
      REFERENCES `jwshop`.`description`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_manufacturer_id` FOREIGN KEY(`manufacturer_id`)
        REFERENCES `jwshop`.`manufacturer`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
-- --------------------------------------------------------------
INSERT INTO `jwshop`.`product` VALUES(DEFAULT, 1, 7, 10000);
INSERT INTO `jwshop`.`product` VALUES(DEFAULT, 1, 8, 20000);
INSERT INTO `jwshop`.`product` VALUES(DEFAULT, 2, 6, 15000);
-- --------------------------------------------------------------
-- --------------------------------------------------------------
CREATE TABLE `jwshop`.`catalog_product` (

  `catalog_id`          BIGINT(20) NOT NULL,
  `product_id`          BIGINT(20) NOT NULL,

  CONSTRAINT `catalog_id` FOREIGN KEY(`catalog_id`)
      REFERENCES `jwshop`.`catalog`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_id` FOREIGN KEY(`product_id`)
      REFERENCES `jwshop`.`product`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
-- --------------------------------------------------------------
INSERT INTO `jwshop`.`catalog_product` VALUES(2, 3);
INSERT INTO `jwshop`.`catalog_product` VALUES(3, 1);
INSERT INTO `jwshop`.`catalog_product` VALUES(3, 2);
-- --------------------------------------------------------------
-- --------------------------------------------------------------
DROP TABLE IF EXISTS `jwshop`.`user_address`;
CREATE TABLE `jwshop`.`user_address` (

  `user_id`             BIGINT(20) NOT NULL,
  `zip`                 VARCHAR(64),
  `country`             VARCHAR(64),
  `state`               VARCHAR(64),
  `city`                VARCHAR(64),
  `street`              VARCHAR(64),
  `number`              VARCHAR(64),

  CONSTRAINT `user_address_pk` PRIMARY KEY(`user_id`, `zip`,`country`, `state`, `city`, `street`, `number`),
  CONSTRAINT `user_address_fk` FOREIGN KEY(`user_id`)
      REFERENCES `jwshop`.`user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
-- --------------------------------------------------------------
INSERT INTO `jwshop`.`user_address` VALUES(4, '000000', 'Ukraine', 'Kharkivska', 'Kharkiv', '23 Avgusta', '33');
INSERT INTO `jwshop`.`user_address` VALUES(4, '000000', 'Ukraine', 'Kharkivska', 'Kharkiv', 'Nauki', '80');
-- --------------------------------------------------------------