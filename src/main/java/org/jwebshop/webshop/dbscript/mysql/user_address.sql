
-- --------------------------------------------------------------
DROP TABLE IF EXISTS `jwshop`.`user_address`;
CREATE TABLE `jwshop`.`user_address` (

  `user_id`             BIGINT(20) NOT NULL,
  `zip`                 VARCHAR(64),
  `country`             VARCHAR(64),
  `state`               VARCHAR(64),
  `city`                VARCHAR(64),
  `street`              VARCHAR(64),
  `number`              VARCHAR(64),

  CONSTRAINT `user_address_pk` PRIMARY KEY(`user_id`, `zip`,`country`, `state`, `city`, `street`, `number`),
  CONSTRAINT `user_address_fk` FOREIGN KEY(`user_id`)
      REFERENCES `jwshop`.`user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
-- --------------------------------------------------------------
INSERT INTO `jwshop`.`user_address` VALUES(4, '000000', 'Ukraine', 'Kharkivska', 'Kharkiv', '23 Avgusta', '33');
INSERT INTO `jwshop`.`user_address` VALUES(4, '000000', 'Ukraine', 'Kharkivska', 'Kharkiv', 'Nauki', '80');
-- --------------------------------------------------------------