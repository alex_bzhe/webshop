package org.jwebshop.webshop.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.jwebshop.webshop.dto.form.ProductFilterForm;
import org.jwebshop.webshop.enumeration.ProductSortOption;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.lang.reflect.Field;
import java.util.HashMap;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SpecificationServiceImpl.class})
class SpecificationServiceImplTest {

    private ProductFilterForm productFilterForm;

    private SpecificationServiceImpl specificationService;

    @BeforeEach
    void setUp() {
        productFilterForm = new ProductFilterForm();
        productFilterForm.setPageNumber(1);
        productFilterForm.setPageSize(5);
        productFilterForm.setProductSortOption(ProductSortOption.PRICE_ASC);
        productFilterForm.setCatalogs(new Long[] {1L, 2L});
        productFilterForm.setManufacturers(new Long[] {1L, 2L});
        productFilterForm.setMaxPrice(20000.0);
        productFilterForm.setMinPrice(0.0);
        productFilterForm.setBrief(null);
    }

    @Test
    void staticSPECSmap_ShouldFireForAllNonNullFieldsOfProductFilterForm() {
        specificationService = PowerMockito.spy(new SpecificationServiceImpl());
        HashMap staticMapMock = PowerMockito.mock(HashMap.class);
        Whitebox.setInternalState(SpecificationServiceImpl.class, "SPECS", staticMapMock);
        int expectedFires = productFilterForm.getClass().getDeclaredFields().length - 1;

        specificationService.getProductSpecifications(productFilterForm);

        verify(staticMapMock, times(expectedFires)).get(any(String.class));
    }

    @Test
    void fieldValueIsPresentMethod_ShouldFireForAllFieldsOfProductFilterForm() {
        specificationService = Mockito.spy(new SpecificationServiceImpl());
        InOrder inOrder = inOrder(specificationService);
        int expectedFires = productFilterForm.getClass().getDeclaredFields().length;

        specificationService.getProductSpecifications(productFilterForm);

        inOrder.verify(specificationService, times(expectedFires)).fieldValueIsPresent(any(Field.class), any(Object.class));
    }

    @Test
    void fieldValueIsPresentMethod_ShouldReturnFalseWhenIncorrectFieldOfObject() throws NoSuchFieldException {
        specificationService = new SpecificationServiceImpl();
        Field incorrectField = productFilterForm.getClass().getDeclaredField("brief");
        boolean actualResult = specificationService.fieldValueIsPresent(incorrectField, new Object());

        assertFalse(actualResult);
    }
}